# Rancher config

### Add the Repo
`helm repo add rancher-stable https://releases.rancher.com/server-charts/stable`

### Create Namespace
`kubectl create namespace cattle-system`

## Setup cert-manager
If you have installed the CRDs manually instead of with the `--set installCRDs=true` option added to your Helm install command, you should upgrade your CRD resources before upgrading the Helm chart:
`kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/<VERSION>/cert-manager.crds.yaml`

### Add the Jetstack Helm repository
`helm repo add jetstack https://charts.jetstack.io`

### Update your local Helm chart repository cache
`helm repo update`

### Install the cert-manager Helm chart
`helm install cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace`

---
### Install rancher
 `helm install rancher rancher-<CHART_REPO>/rancher \
  -f rancher-config.yaml

